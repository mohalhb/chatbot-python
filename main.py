import logging
import sys
#################
import time
import math
import requests



######################
from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 5):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

TRANSPORT,SORTIES,TYPE_SOIREE, RESTAURANTS,ETAT_ITALIEN, CHINOIS,TUNISIEN,THAILANDAIS,MEXICAIN,RETOUR = range(10)

token = sys.argv[1]
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Starts the conversation and asks the user about their gender."""
    reply_keyboard = [["SORTIES", " RESTAURANTS"]]

    await update.message.reply_text(
        "Bonjours ! mon nom est R2D2 Bot.\n"
        "Que voulez vous faire . \n"     
        "voulez vous sortir ou  aller au restaurant  \n\n"
        "faite /cancel pour arreter de me parler  .\n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" SORTIES ou RESTAURANTS  ?"
        ),
    )

    return SORTIES


async def SORTIES(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["Bars", " Clubs", " Musees"],
        ["RETOUR"],
    ]
    #reply_keyboard = [["RETOUR"]]
    await update.message.reply_text(
        "Bon choix ,\n"
        "Nous vous proposons  des Bars ,Clubs , et Musees,\n"
        "A vous de choisire .\n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" Bars Clubs ou Musees ?"
    )
    )

    return TYPE_SOIREE


async def Bars (update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["LA_PANTHERE_OSE", " KLEBER", " ROYAL"],
        ["RETOUR"],
    ]
    await update.message.reply_text(
        "Parfait, \n"
        "voici notre selection de Bars  \n"
        "LA_PANTHERE_OSE \n"
        "KLEBER \n"
        "ROYAL .\n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" LA_PANTHERE_OSE  KLEBER ou ROYAL ?"
        )
    )

    return TYPE_SOIREE

async def Clubs(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["CLUB_LA_MARQUISE", " LE_REXY", " LUX_SHELBY"],
        ["RETOUR"],
    ]
    await update.message.reply_text(
        "PARFAIT  \n"
        "VOICI POUR VOUS NOTRE LISTE DES MEILLEUR CLUB \n"
        " CLUB_LA_MARQUISE \n" 
        "LE_REXY \n"
        "LUX_SHELBY \n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" CLUB_LA_MARQUISE  LE_REXY ou LUX_SHELBY ?"
        )
    )


    return TYPE_SOIREE

async def Musees(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["LOUVRE", " BRITISH_MUSEUM", " NATIONAL_GALLERY"],
        ["RETOUR"],
    ]
    await update.message.reply_text(
        "TRES BON CHOIX ,\n "
        "NOUS VOUS PROPOSONS LE \n"
        "LOUVRE \n "
        "BRITISH_MUSEUM \n "
        "NATIONAL_GALLERY \n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" LOUVRE  BRITISH_MUSEUM ou NATIONAL_GALLERY ?"
        )
    )

    return TYPE_SOIREE

#################################################################################################################################d
async def RESTAURANTS (update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["ITALIEN", " CHINOIS", "TUNISIEN", " THAILANDAIS", "MEXICAIN"],
        ["RETOUR"],
    ]
    await update.message.reply_text(
        "TRES BIEN  \n"
        "QUELLE TYPE DE RESTAURANT SOUHAITEZ VOUS \n"
        "ITALIEN \n"
        "CHINOIS \n"
        "TUNISIEN \n"
        "THAILANDAIS \n"
        "MEXICAIN \n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" ITALIEN  CHINOIS  TUNISIEN THAILANDAIS et MEXICAIN?"
        )
    )
    return RESTAURANTS


async def AFFICHER_ITALIEN (update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [
        ["ITALIEN1", " ITALIEN2", "ITALIEN3"],
        ["RETOUR"],
    ]
   # reply_keyboard = [["RETOUR"]]
    await update.message.reply_text(
        "PARFAIT  \n"
        "VOILA DE TRES BON RESTAURANT ITALIEN \n"
        "ITALIEN1 \n"
        "ITALIEN2 \n"
        "ITALIEN3 \n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" ITALIEN1  ITALIEN2 ou   ITALIEN3 "
        )


    )
    return ETAT_ITALIEN




###################################################################################################################################################33


async def ITALIEN1 (update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_location(46.205884,6.131251)
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    reply_keyboard = [["RETOUR"]]
    await update.message.reply_text(

        "Nom : Les Tilleuls \n "
        "Adress : 2 Avenue Des Tilleuls, Genève 1203 Suisse \n"
        "Tel  : +41 22 344 59 19  \n"
         "Nom : Les Tilleuls \n"
         "CUISINES : Italienne, Méditerranéenne, Européenne \n"
        "  Note \n"
        " Cuisine  : 4.5/5 \n"
        " Service  : 4/5 "
        " Rapport qualité-prix  : 4/5 \n"
        "  Ambiance : 4/5  \n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder=" RETOUR"
        )
    )
    return ETAT_ITALIEN


###################################################################################################################################################33



def call_opendata(path):
    url = f'http://transport.opendata.ch/v1{path}'
    data = requests.get(url)
    return data.json()



# Partie réutilisable

async def send_stations(results, update):
    stations = results['stations']

    message_stops = "Les arrêts qui correspondent à votre recherche:\n"
    for stop in stations:
        if stop['id'] != None:
            stop_name = stop['name']
            stop_id = stop['id']
            message_stops += "\n /" + stop_id + " " + stop_name
            print(stop)

    await update.message.reply_text(message_stops)


# Definir une fonction
async def response_start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int :
    await update.message.reply_text(f'Bienvenue {update.effective_user.first_name}, envoie-moi ta localisation')
    return TRANSPORT


async def response_gps_search(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    location = update.message.location
    results = call_opendata(f'/locations?x={location.longitude}&y={location.latitude}')
    await send_stations(results, update)


async def response_text_search(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    results = call_opendata(f'/locations?query={update.message.text}')
    await send_stations(results, update)


async def response_stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:

    id = update.message.text[1:]
    results = call_opendata(f'/stationboard?id={id}')
    stations = results['stationboard']
    stations = stations[:10]
    text_departures = "Voici les prochains départs à cet arrêt:\n"
    for result in stations:
        text_departures += "\n" + result['operator'] + " " + result['number']
        if result['to']:
            text_departures += " → " + result['to']

        timestamp = result['stop']['departureTimestamp']
        now = time.time()
        diff = timestamp - now
        diff_in_minutes = math.floor(diff / 60)

        if diff_in_minutes < 0:
            text_departures += " déjà parti"
        elif diff_in_minutes < 2:
            text_departures += " bientôt parti"
        else:
            text_departures += f' dans {diff_in_minutes} min.'

    text_departures += "\n\nRecommencer: /" + id

    await update.message.reply_text(text_departures)








###################################################################################################################################################33
async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    await update.message.reply_text(
        "Bye! I hope we can talk again some day.", reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(token).build()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={

            SORTIES: [
                MessageHandler(filters.Regex("^(SORTIES)$"), SORTIES),
                MessageHandler(filters.Regex("^(RESTAURANTS)$"),RESTAURANTS),
                     ],

            TYPE_SOIREE: [
             MessageHandler(filters.Regex("^(RETOUR)$"), start),
             MessageHandler(filters.Regex("^(Bars)$"), Bars),
             MessageHandler(filters.Regex("^(Musees)$"), Musees),
             MessageHandler(filters.Regex("^(Clubs)$"),Clubs),
                        ],

            RESTAURANTS: [
                MessageHandler(filters.Regex("^(RETOUR)$"), start ),
                MessageHandler(filters.Regex("^(ITALIEN)$"), AFFICHER_ITALIEN),
                MessageHandler(filters.Regex("^(CHINOIS)$"), AFFICHER_ITALIEN),
                MessageHandler(filters.Regex("^(TUNISIEN)$"), AFFICHER_ITALIEN),
                MessageHandler(filters.Regex("^(THAILANDAIS)$"), AFFICHER_ITALIEN),
                MessageHandler(filters.Regex("^(MEXICAIN)$"), AFFICHER_ITALIEN),
            ],

            ETAT_ITALIEN: [
                MessageHandler(filters.Regex("^(RETOUR)$"), RESTAURANTS),
                MessageHandler(filters.TEXT, ITALIEN1),
            ],

            CHINOIS: [
                MessageHandler(filters.Regex("^(RETOUR)$"), RESTAURANTS),
                MessageHandler(filters.TEXT, ITALIEN1),
            ],

            TUNISIEN: [
                MessageHandler(filters.Regex("^(RETOUR)$"), RESTAURANTS),
                MessageHandler(filters.TEXT, ITALIEN1),
            ],
            THAILANDAIS: [
                MessageHandler(filters.Regex("^(RETOUR)$"), RESTAURANTS),
                MessageHandler(filters.TEXT, ITALIEN1),
            ],
            MEXICAIN: [
                MessageHandler(filters.Regex("^(RETOUR)$"), RESTAURANTS),
                MessageHandler(filters.TEXT, ITALIEN1),
            ],

            TRANSPORT: [
                CommandHandler("start", response_start),
                MessageHandler(filters.COMMAND, response_stop),
                MessageHandler(filters.LOCATION, response_gps_search),
                MessageHandler(filters.TEXT, response_text_search),

            ],





        },
        fallbacks=[
            CommandHandler("cancel", cancel),
            CommandHandler("transport", response_start),

        ],

    )
    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO

    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()

###################################### #######                      ##########################3####################################
####################################    ##############         ######################################################
#################################          #####################     #################################################







    # Commencer à demander aux serveurs Telegram les nouveaux messages
    app.run_polling()